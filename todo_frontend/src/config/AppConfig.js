export default {
  BASE_URL: process.env.REACT_APP_BASE_URL,
  VERSION: process.env.REACT_APP_VERSION,
};
