import axios from 'axios';
import { toast } from 'react-toastify';

import appConfig from './../config/AppConfig';

const axiosInstance = axios.create({
  baseURL: appConfig.BASE_URL,
});

axiosInstance.interceptors.request.use(
  async (config) => {
    return config;
  },
  (error) => Promise.reject(error)
);

axiosInstance.interceptors.response.use(
  async function (response) {
    return response;
  },
  async function (error) {
    const options = {
      position: 'top-right',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
    };

    if (!error.response) {
      toast.error('Please check your internet connection.', options);
    } else {
      toast.error('Error', options);
    }
    return Promise.reject(error);
  }
);

export default axiosInstance;
