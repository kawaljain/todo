import React, { Component } from 'react';
import { toast } from 'react-toastify';
import { confirmAlert } from 'react-confirm-alert';

import 'react-confirm-alert/src/react-confirm-alert.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/main.css';

import {
  TODO_LIST_TAB,
  TASK_ACTIVE_TAB,
  TASK_COMPLETED,
} from './constant/AppConstant';

import TaskApi from './api/Task';

import TodoLists from './components/TodoLists';
import AddTask from './components/AddTask';
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      taskValue: '',
      taskInput: {
        title: '',
      },
      tasklist: [],
      allTaskList: [],
      isLoading: false,
      activeTab: 3,
      tabList: TODO_LIST_TAB,
      updateTask: {},
      isEdit: false,
    };
  }

  componentDidMount = async () => {
    try {
      const response = await TaskApi.getAll();
      if (response.data.status) {
        this.setState({
          tasklist: response.data.data,
          allTaskList: response.data.data,
        });
      }
    } catch (ex) {}
  };

  onChange = (e) => {
    const task = this.state.taskInput;
    task[e.currentTarget.id] = e.currentTarget.value;
    this.setState({ task: task });
  };

  onTabClick = async (tab) => {
    await this.setState({
      activeTab: tab.value,
    });
    this.showTaskList();
  };

  onEdit = (task) => {
    let taskInput = this.state.taskInput;
    taskInput.title = task.title;
    this.setState({ isEdit: true, updateTask: task });
  };

  addTask = async () => {
    try {
      let allTaskList = this.state.allTaskList;
      let taskInput = this.state.taskInput;
      const response = await TaskApi.create({
        title: taskInput.title,
      });

      if (response.data.status) {
        let task = response.data.data;
        allTaskList.push(task);
        taskInput.title = '';
        this.setState({
          task: taskInput,
          allTaskList: allTaskList,
        });
        toast.success('Task is added successfully');
      }
    } catch (ex) {}
  };

  updateTask = async () => {
    try {
      let task = this.state.updateTask;
      let taskInput = this.state.taskInput;

      let allTaskList = this.state.allTaskList.map((list) => {
        if (task.guid == list.guid) {
          task.title = taskInput.title;
          return task;
        }
        return list;
      });

      const response = await TaskApi.edit({
        title: taskInput.title,
        guid: task.guid,
      });

      if (response.data.status) {
        this.onCancelButton();
        taskInput.title = '';
        await this.setState({ taskInput: taskInput, allTaskList: allTaskList });
        this.showTaskList();
        toast.success('Task is updated successfully');
      }
    } catch (ex) {}
  };

  onDelete = async (task) => {
    confirmAlert({
      title: 'Confirm',
      message: 'Are you sure want to delete this?',
      buttons: [
        {
          label: 'Yes',
          onClick: async () => {
            try {
              let allTaskList = this.state.allTaskList.filter(
                (list) => task.guid != list.guid
              );

              const response = await TaskApi.deleteTask({ guid: task.guid });

              if (response.data.status) {
                await this.setState({ allTaskList });
                this.showTaskList();
                toast.success('Task is deleted successfully');
              }
            } catch (ex) {}
          },
        },
        {
          label: 'No',
          onClick: () => {},
        },
      ],
    });
    return;
  };

  showTaskList = async () => {
    let activeTab = this.state.activeTab;

    if (activeTab == TASK_ACTIVE_TAB) {
      this.setState({
        tasklist: this.state.allTaskList,
      });
    } else {
      let tasklist = this.state.allTaskList.filter((list) => {
        return list.status == activeTab;
      });

      await this.setState({ tasklist });
    }
  };

  markAsDone = async (task) => {
    try {
      let allTaskList = this.state.allTaskList.map((list) => {
        if (task.guid == list.guid) {
          list.status = TASK_COMPLETED;
          return task;
        }
        return list;
      });

      const response = await TaskApi.markAsDone({ guid: task.guid });

      if (response.data.status) {
        await this.setState({ allTaskList: allTaskList });

        this.showTaskList();
        toast.success('Task is mark as done successfully');
      }
    } catch (ex) {}
  };

  onCancelButton = () => {
    let taskInput = this.state.taskInput;
    taskInput.title = '';
    this.setState({ isEdit: false, updateTask: {}, taskInput: taskInput });
  };

  render() {
    return (
      <div className="main-content" style={{ backgroundColor: '#f8f8f8' }}>
        <div className="container">
          <div className="row">
            <div className="col-12" style={{ margin: 20 }}>
              <div className="card card-white">
                <div className="card-body">
                  <div className="head-container">
                    <h1 className="text-center head">To-Do App</h1>
                  </div>
                  <AddTask
                    task={this.state.taskInput}
                    onChange={this.onChange}
                    addTask={this.state.isEdit ? this.updateTask : this.addTask}
                    isEdit={this.state.isEdit}
                    onCancelButton={this.onCancelButton}
                  />

                  {!this.state.isEdit ? (
                    <TodoLists
                      tabList={this.state.tabList}
                      activeTab={this.state.activeTab}
                      onTabClick={this.onTabClick}
                      onEdit={this.onEdit}
                      onDelete={this.onDelete}
                      markAsDone={this.markAsDone}
                      tasklist={this.state.tasklist}
                    />
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
