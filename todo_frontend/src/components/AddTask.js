import { Component } from 'react';
import TaskApi from '../api/Task';
import './../styles/components/AddTask.css';

class AddTask extends Component {
  onChange = (e) => {
    const task = this.state.task;
    task[e.currentTarget.id] = e.currentTarget.value;
    this.setState({ task: task });
  };

  addTask = async () => {
    try {
      const response = await TaskApi.create({
        title: this.state.task.title,
      });

      if (response.data.status) {
        this.setState({
          tasklist: response.data.data,
          allTaskList: response.data.data,
        });
      }
    } catch (ex) {}
  };
  render() {
    let { task, onChange, addTask, onCancelButton, isEdit } = this.props;
    return (
      <div className="add-task-container">
        <div className="add-task-sub-container">
          <input
            placeholder="Add New Task"
            id="title"
            type="text"
            value={task.title}
            onChange={onChange}
            className="form-control "
          />
          <button
            onClick={addTask}
            className="btn btn-primary add-task-button mgLR10"
          >
            {isEdit ? 'Edit ' : 'Add '}Task
          </button>
          {isEdit ? (
            <button
              onClick={onCancelButton}
              className="btn btn-danger delete-task-button mgLR10"
            >
              Cancel Task
            </button>
          ) : null}
        </div>
      </div>
    );
  }
}

export default AddTask;
