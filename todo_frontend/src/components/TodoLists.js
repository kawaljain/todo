import { Component } from 'react';

import './../styles/components/TodoList.css';

import List from '../layout/List';
import Tab from '../layout/Tab';

class TodoLists extends Component {
  render() {
    let {
      tabList,
      activeTab,
      onTabClick,
      onEdit,
      onDelete,
      markAsDone,
      tasklist,
    } = this.props;

    return (
      <div className="todo-list">
        <div className="todo-item">
          <section className="list-container">
            <div className="head-text-container">
              <h2>Todo Lists</h2>
            </div>
            <Tab
              tabList={tabList}
              activeTab={activeTab}
              onTabClick={onTabClick}
            />
            {tasklist && tasklist.length > 0 ? (
              <List
                onEdit={onEdit}
                onDelete={onDelete}
                markAsDone={markAsDone}
                tasks={tasklist}
              />
            ) : (
              <ul className="list-group ">
                <li className="list-group-item text-capitalize  d-flex justify-content-between list-item ">
                  <h6 className="title ">No Task found</h6>
                </li>
              </ul>
            )}
          </section>
        </div>
      </div>
    );
  }
}

export default TodoLists;
