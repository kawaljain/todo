export const TODO_LIST_TAB = [
  {
    value: 3,
    title: 'All',
  },
  {
    value: 1,
    title: 'Active',
  },
  {
    value: 2,
    title: 'Completed',
  },
];
export const TASK_PENDING = 1;
export const TASK_COMPLETED = 2;
export const TASK_ACTIVE_TAB = 3;
