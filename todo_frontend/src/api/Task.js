import Api from './Api';

const url = '/task';
export default class TaskApi extends Api {
  static getAll = async (params) => {
    return Api.getRequest(url + '/get-all');
  };

  static create = async (params) => {
    return Api.postRequest(url + '/create', params);
  };

  static markAsDone = async (params) => {
    return Api.putRequest(url + '/mark-as-done', params);
  };

  static edit = async (params) => {
    return Api.putRequest(url + '/edit', params);
  };

  static deleteTask = async (params) => {
    return Api.putRequest(url + '/delete', params);
  };
}
