import axiosInstance from './../utils/AppAxios';

export default class Api {
  static postRequest = async (url, postData = {}, requestConfig = {}) => {
    const response = await axiosInstance.post(url, postData, requestConfig);
    return response;
  };

  static getRequest = async (url) => {
    const response = await axiosInstance.get(url);
    return response;
  };

  static putRequest = async (url, postData = {}, requestConfig = {}) => {
    const response = await axiosInstance.put(url, postData, requestConfig);
    return response;
  };

  static deleteRequest = async (url, postData = {}, requestConfig = {}) => {
    console.log(postData);
    const response = await axiosInstance.delete(url, postData, requestConfig);
    return response;
  };
}
