import React from 'react';
import './../styles/layout/tab.css';

export default function Tab({ tabList, activeTab = '', onTabClick }) {
  return (
    <>
      <ul className="nav nav-tabs mgBottom20">
        {tabList.length > 0 &&
          tabList.map((tab) => {
            return (
              <li
                className="nav-item"
                key={tab.value}
                onClick={() => onTabClick(tab)}
              >
                <div
                  className={`nav-link ${
                    activeTab === tab.value ? 'active' : ''
                  }`}
                  href="#"
                >
                  {tab.title}
                </div>
              </li>
            );
          })}
      </ul>
    </>
  );
}
