import React from 'react';
import { TASK_COMPLETED } from '../constant/AppConstant';
import './../styles/layout/List.css';

export default function List({
  tasks = [],
  onEdit = () => {},
  onDelete = () => {},
  markAsDone = () => {},
}) {
  return (
    <ul className="list-group ">
      {tasks.length > 0 &&
        tasks.map((task) => {
          return (
            <li
              key={task.guid}
              className="list-group-item text-capitalize d-flex justify-content-between list-item "
            >
              <h6 className="title">{task.title}</h6>
              <div className="action">
                {task.status == TASK_COMPLETED ? null : (
                  <>
                    <span
                      onClick={() => markAsDone(task)}
                      className="u-icon-base info text-info"
                    >
                      <i className="fas fa-check u-icon"></i>
                    </span>
                    <span
                      onClick={() => onEdit(task)}
                      className="u-icon-base success text-success"
                    >
                      <i className="fas fa-pen u-icon"></i>
                    </span>
                  </>
                )}

                <span
                  onClick={() => onDelete(task)}
                  className="u-icon-base danger text-danger"
                >
                  <i className="fas fa-trash u-icon"></i>
                </span>
              </div>
            </li>
          );
        })}
    </ul>
  );
}
