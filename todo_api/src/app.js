const express = require('express');
const app = express();
const helmet = require('helmet');
const cors = require('cors');

const taskRoutes = require('./routes/task.js');

app.use(express.json());
app.use(helmet());
// const corsOptions = {
//   // origin: VALID_ORIGINS,
//   // credentials: true,
// };
app.use(cors());
app.get('/', (req, res) => {
  res.send('App Init');
});

app.use('/api/task', taskRoutes);

module.exports = app;
