const express = require('express');
const { MASK_AS_COMPLETED } = require('../constant/Constant');
const router = express.Router();

const TaskModel = require('./../models/Task');

async function getTaskByGuid(guid) {
  try {
    let [task, _] = await TaskModel.findByGuid(guid);
    if (!task) {
      return false;
    }
    task = task[0];
    return task;
  } catch (error) {
    return false;
  }
}

router.get('/get-all', async (req, res) => {
  try {
    const [response, _] = await TaskModel.findAll();
    res.status(200).json({
      status: true,
      data: response,
    });
  } catch (error) {
    res.status(400).json({
      status: false,
      message: 'Something went wrong. Try after some time.',
    });
  }
});

router.post('/create', async (req, res) => {
  try {
    let data = req.body;

    let taskModel = new TaskModel();

    const validate = await taskModel.validate(data);
    if (validate.error) {
      res.status(400).send({
        status: false,
        message: validate.error.details,
      });
      return false;
    }
    let [newTask, fields] = await taskModel.save(data, true);

    let [insertedData, _] = await TaskModel.findById(newTask.insertId);
    res.status(200).json({
      status: true,
      data: insertedData[0],
    });
  } catch (error) {
    res.status(400).json({
      status: false,
      message: error.message,
    });
  }
  return;
});

router.put('/edit', async (req, res) => {
  try {
    let guid = req.body.guid;

    let task = await getTaskByGuid(guid);
    if (!task) {
      res.status(400).json({ success: false, message: 'Invalid Task' });
      return false;
    }
    let taskModel = new TaskModel();

    const validate = await taskModel.validate({
      title: req.body.title,
    });
    if (validate.error) {
      res.status(400).send({
        status: false,
        message: validate.error.details,
      });
      return false;
    }

    let [updatedTask, fields] = await taskModel.save(
      { title: req.body.title, id: task.id },
      false
    );

    res.status(200).json({
      status: true,
      message: 'Task Updated ',
    });
  } catch (error) {
    res.status(400).json({
      status: false,
      message: error.message,
    });
  }

  return;
});

router.put('/delete', async (req, res) => {
  try {
    let guid = req.body.guid;
    let task = await getTaskByGuid(guid);
    if (!task) {
      res.status(400).json({ status: false, message: 'Invalid Task' });
      return false;
    }
    let response = await TaskModel.deleteById(task.id);
    res.status(200).json({
      status: true,
      message: 'Deleted Successfully.',
    });
  } catch (error) {
    res.status(400).json({ status: false, message: error.message });
  }
  return;
});

router.put('/mark-as-done', async (req, res) => {
  try {
    let guid = req.body.guid;
    let task = await getTaskByGuid(guid);
    if (!task) {
      res.status(400).json({ status: false, message: 'Invalid Task' });
      return false;
    }
    let taskModel = new TaskModel();

    let [updatedTask, fields] = await taskModel.markAsDone(task.id);

    res.status(200).json({
      status: true,
      message: 'Task Updated ',
    });
  } catch (error) {
    res.status(400).json({
      status: false,
      message: error.message,
    });
  }

  return;
});

module.exports = router;
