const Joi = require('joi');
const db = require('./../configuration/db');
let tableName = 'task_list';
const uuid = require('uuid');
const { MASK_AS_COMPLETED } = require('../constant/Constant');

const schema = Joi.object({
  title: Joi.string().min(2).required(),
  status: Joi.number(),
});

class Task {
  constructor() {}

  validate(data) {
    let isNew = true;
    let { title, guid, id } = data;

    if (id) {
      isNew = false;
    }
    const result = schema.validate({ title: title });
    return result;
  }

  generateGuid() {
    return uuid.v4();
  }
  async generateUniqueGuid() {
    let guid = this.generateGuid();
    try {
      while (true) {
        let [response, _] = await Task.findByGuid(guid);
        if (response.length <= 0) {
          break;
        } else {
          guid = this.generateGuid();
        }
      }
    } catch (error) {}
    return guid;
  }

  findByParams(params = []) {}

  async save(data = {}, isNew = true) {
    if (data) {
      if (isNew) {
        let guid = await this.generateUniqueGuid();
        let sql = `INSERT INTO ${tableName} (guid, title) VALUES('${guid}','${data.title}')`;
        return db.execute(sql);
      } else {
        let { title, id } = data;
        let sql = `UPDATE ${tableName} SET TITLE =? WHERE ID =?`;
        return db.execute(sql, [title, id]);
      }
    }
  }

  async markAsDone(taskId) {
    let sql = `UPDATE ${tableName} SET STATUS =? WHERE ID =?`;
    return db.execute(sql, [MASK_AS_COMPLETED, taskId]);
  }

  static findById(taskId, post = []) {
    if (taskId) {
      let sql = `SELECT * FROM ${tableName} WHERE ID = ?`;
      return db.execute(sql, [taskId]);
    }
  }

  static findByGuid(taskGuid, post = []) {
    let sql = `SELECT * FROM ${tableName} WHERE GUID = ?`;
    return db.execute(sql, [taskGuid]);
  }

  static findAll() {
    let sql = `SELECT * FROM ${tableName}`;
    return db.execute(sql);
  }
  static deleteById(taskId, post = []) {
    if (taskId) {
      let sql = `DELETE FROM ${tableName} WHERE ID = ?`;
      return db.execute(sql, [taskId]);
    }
  }
}
module.exports = Task;
